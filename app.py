from flask import Flask, render_template
from dulwich.repo import Repo
import logging
from waitress import serve
import re
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
r = Repo('.')
(_, ref), _ = r.refs.follow(b'HEAD')
match = re.search(r'/([^/]+)$', ref.decode('utf-8'))
branch = match[1]
short_sha = r.head()[:7].decode()


app = Flask(__name__)
@app.route('/', methods=['GET'])
def index():
    logging.info('root route  been accessed')
    return render_template('main.html', response='“Success!”', branch=branch, sha = short_sha, route="root"),200

@app.route('/ping', methods=['GET'])
def ping():
    logging.info('ping route been accessed')
    return render_template('main.html', response='“Ok”', branch=branch, sha = short_sha, route="ping"),200

@app.errorhandler(404)
def not_found(error):
    logging.warning('nonexistent route been accessed')
    response = '''
   _____  _______      _____  
  /  |  | \   _  \    /  |  | 
 /   |  |_/  /_\  \  /   |  |_
/    ^   /\  \_/   \/    ^   /
\____   |  \_____  /\____   | 
     |__|        \/      |__| '''
    return render_template('main.html', response=response, branch=branch, sha = short_sha, route="none"),404

#minimal template with bootom git commit in bottom with two branches for two k8s environments.

if __name__ == "__main__":
    print("The app initiated")
    serve(app, listen='*:8082')