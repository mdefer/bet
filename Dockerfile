FROM python:3-slim
ADD . /app
ADD app.py /app/app.py
WORKDIR /app
RUN pip install -r requirements.txt
ENV PYTHONUNBUFFERED=0
EXPOSE 8082
CMD ["python", "-u", "app.py"]
